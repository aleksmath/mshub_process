#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

"""

import json
from collections import OrderedDict 
def conv2fancy(filename):
    
    json_file    = open(filename,"r")
    json_decoded = json.load(json_file,object_pairs_hook=OrderedDict)
    
    node = []
    # iterate through the nodes
    for lev1_node_id in json_decoded.keys():
        node_l1 = {}
        node_l1['title'] = lev1_node_id
        lev1node      = json_decoded[lev1_node_id]
        if len(lev1node['do'])==2:
            node_l1['checkbox'] = True
        else:
            node_l1['checkbox'] = False
        
        if lev1node['do'][0]=='yes':
            node_l1['selected'] = True
            node_l1['expanded'] = True
        else:
            node_l1['selected'] = False
            node_l1['expanded'] = False
        
        node_l1 = convnode2fancy(node_l1,lev1node) 
        for lev2_node_id in lev1node.keys():
            lev2node = lev1node[lev2_node_id]
            node_l2 = {}
            if isinstance(lev2node,dict):
                node_l2['radiogroup'] = True
                node_l2['expanded']   = True
                node_l2['checkbox']   = False
                node_l2['children']   = []
                node_l2['title']      = lev2_node_id
                selected = 0
                for lev3_node_id in lev2node.keys():
                    node_l3 = {}
                    if selected==0:
                        node_l3['selected'] = True
                        selected = 1
                    node_l3['expanded']   = True
                    node_l3['checkbox']   = "radio"
                    node_l3['children']   = []
                    node_l3['title']      = lev3_node_id
                    lev3node = lev2node[lev3_node_id]
                    node_l3 = convnode2fancy(node_l3,lev3node)
                    node_l2['children'].append(node_l3)
                node_l1['children'].append(node_l2)
                
        node.append(node_l1)
        
        
    with open('ftree_'+filename, 'w') as outfile:
        json.dump(node, outfile, sort_keys=False)
        
        
def convnode2fancy(node_l1,lev1node):
    node_l1['children'] = []
    for lev2_node_id in lev1node.keys():
        if lev2_node_id=='do':
                continue
        lev2node = lev1node[lev2_node_id]
        node_l2 = {}
        node_l2['title']    = lev2_node_id
        node_l2['expanded'] = True
        node_l2['checkbox'] = False
        node_l2['children'] = []
        # go deeper
        if isinstance(lev2node,list):
           node_l2['radiogroup'] = True
           node_l2['checkbox'] = False
           selected = 0
           for ilev2node in lev2node:   
                node_l3 = {}
                node_l3['title']    = ilev2node
                node_l3['expanded'] = True
                node_l3['checkbox'] = "radio"
                if selected==0:
                    node_l3['selected'] = True
                    selected = 1
                node_l2['children'].append(node_l3)
        elif isinstance(lev2node,dict):
          continue
                
        else:
            node_l3 = {}
            node_l3['title']    = lev2node
            node_l3['expanded'] = True
            node_l3['radiogroup'] = False
            node_l3['checkbox'] = False
            node_l2['children'].append(node_l3)
          #    node_l2['children'] = []
        node_l1['children'].append(node_l2)
    return node_l1

if __name__ == "__main__":
    conv2fancy('GC_MS_simplified.json')
    