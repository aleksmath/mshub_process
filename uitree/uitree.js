var Source = [  // Typically we would load using ajax instead...
      {title: "vxvzxvzxv 1",checkbox: false,  folder: true, selected:true, expanded: true},
      {title: "Node 2"},
      {title: "Folder 3",  folder: true, checkbox: true, radiogroup: true, selected:true, expanded: true, children: [
        {title: "Node 3.1", folder: false, selected: true, checkbox: "radio"},
        {title: "Node 3.2", folder: true,checkbox: "radio"}
      ]},
      {title: "Folder 4", folder: true, selected:true, expanded: true, checkbox: true, radiogroup: true, children: [
        {title: "Node 4.1",selected: true},
        {title: "Node 4.2"}
      ]}
      ]

$(function(){
 $("#tree2").fancytree({
 	  extensions: ["edit"],
      checkbox: true,
      selectMode: 2,
      source: {url:'ftree_GC_MS_simplified.json'},
      icon: function(event, data) {
  	   { return "none"; }
		},
      init: function(event, data) {
        // Set key from first part of title (just for this demo output)
        data.tree.visit(function(n) {
          n.key = n.title.split(" ")[0];
        });
      },
      select: function(event, data) {
        // Display list of selected nodes
        var selNodes = data.tree.getSelectedNodes();
        // convert to title/key array
        var selKeys = $.map(selNodes, function(node){
             return "[" + node.key + "]: '" + node.title + "'";
          });
        $("#echoSelection2").text(selKeys.join(", "));
      },
       edit: {
        triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"],
        beforeEdit: function(event, data){
          if( data.node.getLevel()==1 || data.node.radiogroup==true
             || data.node.getLevel()==2||data.node.checkbox=="radio"||
             data.node.getLevel()==4){
        	return false;}
        },
        edit: function(event, data){
          // Editor was opened (available as data.input)
        },
        beforeClose: function(event, data){
          // Return false to prevent cancel/save (data.input is available)
          console.log(event.type, event, data);
          if( data.originalEvent.type === "mousedown" ) {
            // We could prevent the mouse click from generating a blur event
            // (which would then again close the editor) and return `false` to keep
            // the editor open:
  //                  data.originalEvent.preventDefault();
  //                  return false;
            // Or go on with closing the editor, but discard any changes:
  //                  data.save = false;
          }
        },
        save: function(event, data){
          // Save data.input.val() or return false to keep editor open
          console.log("save...", this, data);
          // Simulate to start a slow ajax request...
          setTimeout(function(){
            $(data.node.span).removeClass("pending");
            // Let's pretend the server returned a slightly modified
            // title:
            data.node.setTitle(data.node.title);
          }, 2000);
          // We return true, so ext-edit will set the current user input
          // as title
          return true;
        },
        close: function(event, data){
          // Editor was removed
          if( data.save ) {
            // Since we started an async request, mark the node as preliminary
            $(data.node.span).addClass("pending");
          }
      }
    }
    });
});    