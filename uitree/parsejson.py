import json

def parse_fancy_params(jsonurl=None):

    def get_children(parentnode):
        if not 'children' in parentnode:
            get_params(parentnode, modulename)
        else:
            for child in parentnode['children']:
                get_params(child, modulename)

    def get_params(child, modulename):
        # if not child['selected']:
        #     return
        # elif child['selected']:
        #     if child['title'] in intermap:
        #         mappedtitle = intermap[child['title']]
        #     else:
        #         mappedtitle = child['title']
        #
        #     if type(mappedtitle) == list:
        #         moduleparams[modulename].extend(mappedtitle)
        #     else:
        #         moduleparams[modulename].append(mappedtitle)
        #     if 'children' in child:
        #         get_children(child)
        #     else:
        #         return

        if child['title'] in intermap:
            mappedtitle = intermap[child['title']]
        else:
            mappedtitle = child['title']

        if not child['selected']:
            if child['checkbox'] == "radio":
                return
            else:
                if type(mappedtitle) == list:
                    moduleparams[modulename].extend(mappedtitle)
                    moduleparams[modulename].append('none')
                elif type(mappedtitle) == tuple:
                    moduleparams[modulename].extend(mappedtitle[1])
                else:
                    moduleparams[modulename].append(str(mappedtitle))
                    moduleparams[modulename].append('none')
                return

        elif child['selected']:
            if type(mappedtitle) == list:
                moduleparams[modulename].extend(mappedtitle)
            elif type(mappedtitle) == tuple:
                moduleparams[modulename].extend(mappedtitle[0])
            else:
                moduleparams[modulename].append(str(mappedtitle))

            if 'children' in child:
                get_children(child)
            else:
                return

    intermap = {"Intra-sample m/z peak drift correction": "intrapalign.py",
                "Noise filtering and baseline correction": "noisefilter.py",
                "Inter-sample retention time alignment": "interpalign.py",
                "Peak Detection and Fragmentation Pattern Extraction": "peakdetect.py",
                "Smoothing": "--smoothmethod",
                "Baseline Correction": "--baselinemethod",
                "Binning": ["--method", "binning"],
                "Bin Size": "--binsize",
                "Bin Shift": "--binshift",
                "Window": "--window",
                "SQfilter": "sqfilter",
                "Binning Units": "--units",
                "Top Hat": "tophat",
                "Frame": "--frame",
                "RSPA": ["--method", "rspa"],
                "Recursion": (["--recursion", "1"], ["--recursion", "0"]),
                "Reference": "--reference",
                "Mean": "mean",
                "Median": "median",
                "MinSegWidth": "--minsegwidth",
                "MaxPeakShift": "--maxpeakshift",
                "Gap": "--min_width",
                "Smoothderiv": ["--peak_detect_method", "smoothderiv"]}

    moduleparams = {}
    with open(jsonurl, 'r') as readfile:
        jsondata = json.loads(readfile.read())

    for module in jsondata:
        title = module['title']
        modulename = intermap[title]

        if module['selected']:
            moduleparams[intermap[title]] = []
            get_children(module)
        else:
            continue

    return moduleparams

if __name__ == "__main__":
    modelsparams = parse_fancy_params(jsonurl="/Users/Dieter/Desktop/MSHub_final/MSHub/static/defaults/default_pipes/default_GCMS.json")
    print('done')