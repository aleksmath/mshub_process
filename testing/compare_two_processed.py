# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 22:19:18 2017

@author: ilaponog
"""

import numpy as np;
import os;


class DataHolder:
    
    def __init__(self, master_list, int_list):
        self.load_data(master_list, int_list);
        
    def load_data(self, master_list, int_list):
        self._load_master_list(master_list);
        self._load_int_list(int_list);
        
    def _load_master_list(self, master_list):
     
        self.rt_peaks = [];
        self.ms_fragments = [];
        reading_peaks = False;
        n_expected_peaks = 0;
        current_ms_fragments = [];
        with open(os.path.abspath(master_list), 'r') as finp:
            for s in finp:
                s = s.rstrip('\n');
                if reading_peaks:
                    s = s.split(';');
                    for block in s:
                        if (' ' in block) and reading_peaks:
                            subblocks = block.lstrip().rstrip().split(' ');
                            current_ms_fragments[0].append(float(subblocks[0]));
                            current_ms_fragments[1].append(float(subblocks[1]));
                            if len(current_ms_fragments[0]) >= n_expected_peaks:
                                self.ms_fragments.append(current_ms_fragments);
                                reading_peaks = False;
                                n_expected_peaks = 0;
                                current_ms_fragments = [];
                                break;
                        elif not reading_peaks:
                            break;
                elif s.startswith('Name:'):
                    rt = s[s.index('time_') + 5:s.index('_min')];
                    self.rt_peaks.append(float(rt));
                elif s.startswith('Num Peaks:'):
                    num_p = s[s.index(':') + 1 :];
                    reading_peaks = True;
                    n_expected_peaks = int(num_p);
                    current_ms_fragments.append([]);
                    current_ms_fragments.append([]);
        
        #correcting for identical sequential values for proper alignment and matching
        for i in range(len(self.rt_peaks) - 1):
            if self.rt_peaks[i] == self.rt_peaks[i + 1]:
                self.rt_peaks[i] = self.rt_peaks[i] - 0.02;
                self.rt_peaks[i + 1] = self.rt_peaks[i + 1] + 0.02;

        for i in range(len(self.ms_fragments)):
            v = self.ms_fragments[i][0]
            for j in range(len(v) - 1):
                if v[j] == v[j + 1]:
                    v[j] = v[j] - 0.02;
                    v[j + 1] = [j + 1] + 0.02;
        
        self.rt_peaks = np.array(self.rt_peaks, dtype = np.float64);
        
        
    def _load_int_list(self, int_list):
        self.sample_ids = [];
        self.integrals = [];
        with open(os.path.abspath(int_list), 'r') as finp:
            index = -1;
            for s in finp:
                index += 1;
                s = s.rstrip('\n').split(',');
                if index > 0:
                    if s[0].upper().endswith('.CDF'):
                        self.sample_ids.append(s[0][:-4]);
                    else:
                        self.sample_ids.append(s[0]);
                    for i in range(1, len(s)):
                        if s[i] == 'NA':
                            s[i] = np.nan;
                    self.integrals.append(s[1:]);
                
        self.integrals = np.array(self.integrals, dtype = np.float64);
        self.integrals[self.integrals == 0.0] = np.nan;
        self.integrals = np.nan_to_num(self.integrals);


#-----------------------------------------------------        
    
def match_lists(list1, list2, return_inverse = False):
    """
    Matching of two lists.
    
    Args:
        list1, list2  - two lists to be matched
        return_inverse - return inverse conversion indeces or not, default = False
        
    Returns:
        indeces1 - array of conversion indeces (dtype = np.int64) 
                      from list1 to list2, i.e. for i in range(len(list1))
                      list1[i] is matched to list2[indeces1[i]],
                      indeces1 has same size as list1
                      "-1" numerical value in indeces1[i] indicates no match
        
        indeces2 - (optional) array of inverse conversion indeces 
                      (dtype = np.int64) i.e. for i in range(len(list2))
                      list2[i] is matched to list1[indeces2[i]],
                      indeces2 has same size as list2
                      "-1" numerical value in indeces2[i] indicates no match
    
    """
    indeces1 = [];
    for s in list1:
        try:
            #for each element in list1 try to find corresponding index 
            #in list2
            indeces1.append(list2.index(s));
        except:
            #if none found - set index to -1
            indeces1.append(-1);
    
    #convert to numpy array
    indeces1 = np.array(indeces1, dtype = np.int64);

    if return_inverse:
        #prepare output array indeces2 pre-filled with -1 for no matches
        indeces2 = np.full((len(list2), ), -1, dtype = np.int64);
        #get mask of matched indeces
        list1_mask = indeces1 >= 0;
        #get indeces of matches
        list1_indcs = np.arange(len(list1), dtype = np.int64);
        #assign them to inverse indeces
        indeces2[indeces1[list1_mask]] = list1_indcs[list1_mask];
        return indeces1, indeces2;
    else:
        return indeces1

def nn_match(rt, crt, tolerance = 0.1001, return_inverse = False):
    """
    Recursive matching of two 1D NumPy arrays of values with given tolerance.
    
    Args:
        rt, crt - two 1D NumPy arrays to be matched
        tolerance - allowed matching distance between values, default = 0.1001
        return_inverse - return inverse conversion indeces or not, default = False
        
    Returns:
        rt2crtindcs - array of conversion indeces (dtype = np.int64) 
                      from rt to crt, i.e. for i in range(rt.shape[0])
                      rt[i] is matched to crt[rt2crtindcs[i]],
                      rt2crtindcs has same size as rt
                      "-1" numerical value in rt2crtindcs[i] indicates no match
                      with given tolerance
        
        crt2rtindcs - (optional) array of inverse conversion indeces 
                      (dtype = np.int64) i.e. for i in range(crt.shape[0])
                      crt[i] is matched to rt[crt2rtindcs[i]],
                      crt2rtindcs has same size as crt
                      "-1" numerical value in crt2rtindcs[i] indicates no match
                      with given tolerance
    
    """
    
    if len(rt.shape) != 1:
        raise TypeError('rt should be 1D NumPy array!');

    if len(crt.shape) != 1:
        raise TypeError('crt should be 1D NumPy array!');
        
    #indeces for array subselections    
    crt_ind_range = np.arange(crt.shape[0], dtype = np.int64);
    rt_ind_range = np.arange(rt.shape[0], dtype = np.int64);

    # efficient nearest-neighbour alignment of retention time vector to the common feature vector
    rt2crtindcs = np.round(np.interp(rt, crt, np.arange(0., len(crt))))
    rt2crtindcs = (rt2crtindcs.astype(int)).flatten()
                
    # remove all matched pairs smaller than pre-defined or calculated torelance 
    # -1 indicates no match
    rt2crtindcs[np.abs(crt[rt2crtindcs] - rt) > tolerance] = -1;
    
    # remove repetitions leaving the closest match only
    #get unique link indeces and their counts
    u, uc = np.unique(rt2crtindcs, return_counts = True);
    #get indeces with counts more than 1
    mids = u[uc > 1];
    #exclude mismatched indeces marked as -1
    mids = mids[mids >=0 ];
    
    #prepare the list for indeces to be re-processed recursively
    redo_list = [];
    
    #Iterate through indeces with multiple matches (i.e. non-unique ones)    
    for i in range(mids.shape[0]):
        #get rt indeces of the non-unique match
        rti = rt_ind_range[rt2crtindcs == mids[i]];
        #calculate distances for non-unique matches
        dist = np.abs(crt[mids[i]] - rt[rti]);
        #get position of the best match
        best = np.argmin(dist);
        #set all indeces to -2 to indicate the non-unique match
        rt2crtindcs[rti] = -2;
        #but set the best match to one closest in dist
        rt2crtindcs[rti[best]] = mids[i];
        #add the remaining indeces to the list of the ones needing re-iteration
        redo_list.append(np.delete(rti, best));

    #check if re-iteration is needed (i.e. there are non-matched non-unique entries left)    
    if redo_list:
        #get full list of indeces for re-matching
        redo_rt_idcs = np.hstack(redo_list);
        #prepare boolean mask for unmatched indeces
        unmatched_crts = np.full(crt.shape, True, dtype = np.bool);
        #get the list of uniquely matched indeces
        matched_crts = rt2crtindcs[rt2crtindcs >= 0];
        #exclude entries which were uniquely matched from the mask
        unmatched_crts[matched_crts] = False;
        #get the list of unmatched indeces
        redo_crt_idcs = crt_ind_range[unmatched_crts];
        #call matching recursively for unmatched and non-uniquely matched indeces
        sub_rt2crt = nn_match(rt[redo_rt_idcs], crt[redo_crt_idcs], tolerance);
        #get mask for newly matched indeces
        sub_rt2crt_unmatched = sub_rt2crt < 0;
        #get inverse mask for completely unmatched indeces
        sub_rt2crt_matched = np.logical_not(sub_rt2crt_unmatched);
        #set completely unmatched indeces to -1
        rt2crtindcs[redo_rt_idcs[sub_rt2crt_unmatched]] = -1;
        #set re-matched indeces to the newly detected values from recursive call
        rt2crtindcs[redo_rt_idcs[sub_rt2crt_matched]] = redo_crt_idcs[sub_rt2crt[sub_rt2crt_matched]];
        
    if return_inverse:
        #create and pre-fill crt2rtindcs with -1 to cover non-matched indeces
        crt2rtindcs = np.full(crt.shape, -1, dtype = np.int64);
        #get mask of matched rt indeces
        match_rt2crt_mask = rt2crtindcs >=0;
        #get array of matched crt indeces
        matched_crts = rt2crtindcs[match_rt2crt_mask];
        #get array of corresponding rt indeces
        matched_rts = rt_ind_range[match_rt2crt_mask];
        #assign matched inverse indeces
        crt2rtindcs[matched_crts] = matched_rts;
        return rt2crtindcs, crt2rtindcs;
    else:
        return rt2crtindcs;



#------------------------------


def get_cmz_stats(cmz1, cmz2):
    data = np.zeros((8,), dtype = np.float64);
    
    mz1 = np.array(cmz1[0]);
    i1 = np.array(cmz1[1]);
    
    mz2 = np.array(cmz2[0]);
    i2 = np.array(cmz2[1]);
    
    cmz_match = nn_match(mz1, mz2, tolerance = 0.4);
    
    mask = cmz_match >= 0;
    cmz_ind1 = np.arange(mz1.shape[0], dtype = np.int64)[mask];
    cmz_ind2 = cmz_match[mask];
    
    data[0] = mz1.shape[0];
    data[1] = mz2.shape[0];
    data[2] = cmz_ind1.shape[0];
    data[3] = data[2]/mz1.shape[0]*100.0;
    data[4] = data[2]/mz2.shape[0]*100.0;
    
    oi1 = i1[cmz_ind1];
    oi2 = i2[cmz_ind2];

    data[5] = np.sum(oi1)/np.sum(i1)*100.0;
    data[6] = np.sum(oi2)/np.sum(i2)*100.0;
    
    data[7] = np.corrcoef(oi1, oi2)[0, 1];
    if np.isnan(data[7]):
        data[7] = 0.0;
    
    
    return data
    
def get_integral_correlations(our_integrals, their_integrals):
    
    corr_sample = np.zeros((our_integrals.shape[0],), dtype = np.float64);
    corr_rt = np.zeros((our_integrals.shape[0],), dtype = np.float64);
    corr_all = np.corrcoef(our_integrals.flatten(), their_integrals.flatten())[0, 1];
    
    for i in range(corr_sample.shape[0]):
        corr_sample[i] = np.corrcoef(our_integrals[i, :], their_integrals[i, :])[0, 1];
    
    for i in range(corr_rt.shape[0]):
        corr_rt[i] = np.corrcoef(our_integrals[:, i], their_integrals[:, i])[0, 1];

    return corr_sample, corr_rt, corr_all    
    
    
if __name__ == "__main__": 
    
    
    master_list_xcms  = 'e:/FTP/GCMS/testData/Sample_data_for_50_patients/Meta/master_peak_list.txt';
    master_list_mshub = 'e:/FTP/GCMS/testData/Sample_data_for_50_patients/Results/Sample_data_for_50_patients_ms_peaks.txt';
    
    int_list_xcms = 'e:/FTP/GCMS/testData/Sample_data_for_50_patients/Meta/TIC_table.csv';
    int_list_mshub = 'e:/FTP/GCMS/testData/Sample_data_for_50_patients/Results/Sample_data_for_50_patients_integrals.csv';
    
    outfile = 'e:/FTP/GCMS/testData/Sample_data_for_50_patients/Results/testoverlap_TIC_2.csv';





    #h5file = 'e:/FTP/GCMS/testData/Faisal_21062017/Results/Faisal_21062017.h5';
    
    
    
    #print(match_ranges(np.array([0.1, 0.2, 0.23, 0.3, 0.4]), np.array([0, 0.1, 0.2, 0.21, 0.4, 0.5]), 0.05))
    #print(nn_match(np.array([0.1, 0.2, 0.23, 0.3, 0.4]), np.array([0, 0.1, 0.2, 0.21, 0.4, 0.5]), 0.05, return_inverse = True))
    
    our_data = DataHolder(master_list_mshub, int_list_mshub);
    their_data = DataHolder(master_list_xcms, int_list_xcms);
    
    #our_to_their, their_to_our = match_ranges(our_data.rt_peaks, their_data.rt_peaks, 0.1001);    
    our_to_their, their_to_our = nn_match(our_data.rt_peaks, their_data.rt_peaks, 0.1001, return_inverse = True);    
    
    #print(np.all(our_to_their == our_to_their2))
    #print(np.all(their_to_our == their_to_our2))
    
    d1to2, d2to1 = match_lists(our_data.sample_ids, their_data.sample_ids, return_inverse = True);    
    
    #print(our_to_their)
    #print(their_to_our)
    #print(len(our_to_their[our_to_their >= 0]))
    #print(len(their_to_our[their_to_our >= 0]))
    
    dour_to_their = d1to2[d1to2 >= 0];
    dtheir_to_our = d2to1[d2to1 >= 0];
    
    print('Their datasets: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(their_data.sample_ids), len(dtheir_to_our), float(len(dtheir_to_our))/len(their_data.sample_ids)*100.0));
    print('Our datasets: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(our_data.sample_ids), len(dour_to_their), float(len(dour_to_their))/len(our_data.sample_ids)*100.0));

    print('Their rt peaks: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(their_data.rt_peaks), len(their_to_our[their_to_our >= 0]), float(len(their_to_our[their_to_our >= 0]))/len(their_data.rt_peaks)*100.0));
    print('Our rt peaks: Count %s, Overlap: %s, Percentage in overlap: %3.1f%%'%(
        len(our_data.rt_peaks), len(our_to_their[our_to_their >= 0]), float(len(our_to_their[our_to_their >= 0]))/len(our_data.rt_peaks)*100.0));
    
    #retention time match    
    mask = our_to_their >= 0;    
    match1 = np.arange(our_to_their.shape[0], dtype = np.int64)[mask];
    match2 = our_to_their[mask];
    
    
    #datasets match
    mask = d1to2 >= 0;
    dmatch1 = np.arange(d1to2.shape[0], dtype = np.int64)[mask];
    dmatch2 = d1to2[mask];
    
    
    #print(match1)
    #print(match2)
    
    with open(outfile, 'w') as fout:
        fout.write('Their datasets:\n%s\n%s\n\n'%(master_list_xcms, int_list_xcms));
        fout.write('Our datasets:\n%s\n%s\n\n'%(master_list_mshub, int_list_mshub));
        
        fout.write('Their datasets:, Count:, %s, Overlap:, %s, Percentage in overlap:, %3.1f%%\n'%(
            len(their_data.sample_ids), len(dtheir_to_our), float(len(dtheir_to_our))/len(their_data.sample_ids)*100.0));
        fout.write('Our datasets:, Count:, %s, Overlap:, %s, Percentage in overlap:, %3.1f%%\n'%(
            len(our_data.sample_ids), len(dour_to_their), float(len(dour_to_their))/len(our_data.sample_ids)*100.0));

        fout.write('\n');
    
        fout.write('Their rt peaks:, Count:, %s, Overlap: %s, Percentage in overlap:, %3.1f%%\n'%(
            len(their_data.rt_peaks), len(their_to_our[their_to_our >= 0]), float(len(their_to_our[their_to_our >= 0]))/len(their_data.rt_peaks)*100.0));
        fout.write('Our rt peaks:, Count:, %s, Overlap: %s, Percentage in overlap:, %3.1f%%\n'%(
            len(our_data.rt_peaks), len(our_to_their[our_to_their >= 0]), float(len(our_to_their[our_to_their >= 0]))/len(our_data.rt_peaks)*100.0));

        fout.write('\n');
            
        fout.write('Overlapping peaks (our/their):');
        for i in range(match1.shape[0]):
            fout.write(', %s'%our_data.rt_peaks[match1[i]]);
        fout.write('\n');

        for i in range(match1.shape[0]):
            fout.write(', %s'%their_data.rt_peaks[match2[i]]);
        fout.write('\n');

        fout.write('\n');
        
        #CMZ section
        
        fout.write('Fragmentation peak match:\n');
        data = np.zeros((match1.shape[0], 8), dtype = np.float64);
        for i in range(match1.shape[0]):
            data[i, :] = get_cmz_stats(our_data.ms_fragments[match1[i]], their_data.ms_fragments[match2[i]]);

        fout.write('N our:');
        for i in range(match1.shape[0]):
            fout.write(', %s'%int(data[i, 0]));
        fout.write('\n');
            
        fout.write('N their:');
        for i in range(match1.shape[0]):
            fout.write(', %s'%int(data[i, 1]));
        fout.write('\n');

        fout.write('N overlap:');
        for i in range(match1.shape[0]):
            fout.write(', %s'%int(data[i, 2]));
        fout.write('\n');

        fout.write('%% our:');
        for i in range(match1.shape[0]):
            fout.write(', %3.1f'%(data[i, 3]));
        fout.write('\n');

        fout.write('%% their:');
        for i in range(match1.shape[0]):
            fout.write(', %3.1f'%(data[i, 4]));
        fout.write('\n');

        fout.write('%% integral our:');
        for i in range(match1.shape[0]):
            fout.write(', %3.3f'%int(data[i, 5]));
        fout.write('\n');

        fout.write('%% integral their:');
        for i in range(match1.shape[0]):
            fout.write(', %3.3f'%int(data[i, 6]));
        fout.write('\n');

        fout.write('Correlation %%:');
        for i in range(match1.shape[0]):
            fout.write(', %3.4f'%(data[i, 7]*100.0));
        fout.write('\n');

        fout.write('\n');
        
        fout.write(',Average, std\n');
        meandata = np.mean(data, axis = 0);
        stddata = np.std(data, axis = 0);
        


        fout.write('N our:, %s, %s\n'%(meandata[0], stddata[0]));
        fout.write('N their:, %s, %s\n'%(meandata[1], stddata[1]));
        fout.write('N overlap:, %s, %s\n'%(meandata[2], stddata[2]));
        fout.write('%% our:, %s, %s\n'%(meandata[3], stddata[3]));
        fout.write('%% their:, %s, %s\n'%(meandata[4], stddata[4]));
        fout.write('%% integral our:, %s, %s\n'%(meandata[5], stddata[5]));
        fout.write('%% integral their:, %s, %s\n'%(meandata[6], stddata[6]));
        fout.write('Correlation %%:, %s, %s\n'%(meandata[7], stddata[7]));

        fout.write('\n');
        
        fout.write('Weighted with our integrals:\n');
        integrals1 = np.sum(our_data.integrals, axis = 0)[match1];
        
        sumintegral1 = np.mean(integrals1);
        
        mul = np.multiply(data.transpose(), integrals1);
        meandata = np.mean(mul, axis = 1)/sumintegral1;
        
        stddata = np.power(stddata, 2);
        i2 = np.sum(np.power(integrals1, 2)) / np.power(np.sum(integrals1), 2);
        stddata = np.sqrt(stddata * i2);

        fout.write('N our:, %s, %s\n'%(meandata[0], stddata[0]));
        fout.write('N their:, %s, %s\n'%(meandata[1], stddata[1]));
        fout.write('N overlap:, %s, %s\n'%(meandata[2], stddata[2]));
        fout.write('%% our:, %s, %s\n'%(meandata[3], stddata[3]));
        fout.write('%% their:, %s, %s\n'%(meandata[4], stddata[4]));
        fout.write('%% integral our:, %s, %s\n'%(meandata[5], stddata[5]));
        fout.write('%% integral their:, %s, %s\n'%(meandata[6], stddata[6]));
        fout.write('Correlation %%:, %s, %s\n'%(meandata[7], stddata[7]));

        fout.write('\n');
        
        fout.write('Weighted with their integrals:\n');

        meandata = np.mean(data, axis = 0);
        stddata = np.std(data, axis = 0);
        
        integrals1 = np.sum(their_data.integrals, axis = 0)[match2];
        
        sumintegral1 = np.mean(integrals1);

        mul = np.multiply(data.transpose(), integrals1);
        meandata = np.mean(mul, axis = 1)/sumintegral1;
        
        stddata = np.power(stddata, 2);
        i2 = np.sum(np.power(integrals1, 2)) / np.power(np.sum(integrals1), 2);
        stddata = np.sqrt(stddata * i2);

        fout.write('N our:, %s, %s\n'%(meandata[0], stddata[0]));
        fout.write('N their:, %s, %s\n'%(meandata[1], stddata[1]));
        fout.write('N overlap:, %s, %s\n'%(meandata[2], stddata[2]));
        fout.write('%% our:, %s, %s\n'%(meandata[3], stddata[3]));
        fout.write('%% their:, %s, %s\n'%(meandata[4], stddata[4]));
        fout.write('%% integral our:, %s, %s\n'%(meandata[5], stddata[5]));
        fout.write('%% integral their:, %s, %s\n'%(meandata[6], stddata[6]));
        fout.write('Correlation %%:, %s, %s\n'%(meandata[7], stddata[7]));

        fout.write('\n');
        fout.write('\n');
        
        i1 = our_data.integrals[dmatch1, :];
        i2 = their_data.integrals[dmatch2, :];
        
        i1 = i1[:, match1];
        i2 = i2[:, match2];
        
        corr_sample, corr_rt, corr_all = get_integral_correlations(i1, i2);
        
        fout.write('Percent of overlapped total signal:\n');
        
        fout.write('Our: , %s\n'%(np.sum(i1)/np.sum(our_data.integrals)*100.0))
        fout.write('Their: , %s\n'%(np.sum(i2)/np.sum(their_data.integrals)*100.0))

        fout.write('\n');
        fout.write('Correlation in overlapped total signal:\n');
        fout.write('%s'%(corr_all*100));

        fout.write('\n');
        
        fout.write('Per Sample Correlation:\n');
        fout.write('N, Sample, Correlation %%, Sum our intensity in overlap, Sum their intensity in overlap:\n');
        
        for i in range(corr_sample.shape[0]):
            fout.write('%s,%s,%s, %s, %s\n'%(i+1, our_data.sample_ids[dmatch1[i]], corr_sample[i]*100, np.sum(i1[i, :]), np.sum(i2[i, :])))
        
        fout.write('\n');
        
        fout.write('Per RT Correlation:\n');
        fout.write('N, RT, Correlation %%, Sum our intensity in overlap, Sum their intensity in overlap:\n');

        for i in range(corr_rt.shape[0]):
            fout.write('%s,%s,%s, %s, %s\n'%(i+1, our_data.rt_peaks[match1[i]], corr_rt[i]*100, np.sum(i1[:, i]), np.sum(i2[:, i])))
        
        fout.write('\n');
